import { useContext } from 'react';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import { CardActionArea} from '@mui/material';
import ThemeContext from '../context/Theme';

export default function FlagCard({ imgSrc, name, population, region, capital, id }) {

    const { darkMode } = useContext(ThemeContext);

    return (
        <Card sx={{ height: '320px', background: darkMode === 'true' ? 'var(--dark-secondary)' : 'var(--light-secondary)' }}>
            <CardActionArea sx={{ width: '280px' }} >
                <CardMedia sx={{ objectFit: "cover" }}
                    component="img"
                    height="140"
                    image={imgSrc}
                    alt="not found"
                />
                <CardContent sx={{
                    padding: '32px 16px', background: darkMode === 'true' ? 'var(--dark-secondary)' : 'var(--light-secondary)',
                    color: darkMode === 'true' ? 'var(--light-primary)' : 'var(--light-text)',
                }}>
                    <Typography gutterBottom variant="h5" component="div" >
                        {name}
                    </Typography>
                    <Typography >
                        <b>Population: </b> {population}
                    </Typography>
                    <Typography>
                        <b>Region: </b> {region}
                    </Typography>
                    <Typography>
                        <b>Capital: </b> {capital}
                    </Typography>
                </CardContent>
            </CardActionArea>
        </Card>
    );
}