import React, { useState, useContext } from 'react'
import { AppBar, Box, Toolbar, Typography, Button } from '@mui/material';
import ThemeContext from '../context/Theme';
import LightModeIcon from '@mui/icons-material/LightMode';
import DarkModeIcon from '@mui/icons-material/DarkMode';

const Navbar = () => {

    const { darkMode, setDarkMode } = useContext(ThemeContext);

    const toggleMode = () => {
        setDarkMode(darkMode === 'false' ? 'true' : 'false')
    }



    return (
        <>
            <Box sx={{ position: 'relative', zIndex: '10' }}>
                <AppBar position="static">
                    <Toolbar color="var(--light-text)" sx={{
                        background: darkMode === 'true' ? 'var(--dark-secondary)' : 'var(--light-secondary)',
                        color: darkMode === 'true' ? 'var(--light-primary)' : 'var(--light-text)', height: "80px", zIndex: '10'
                    }} >
                        <Typography variant="h5" component="div" sx={{ flexGrow: 1, fontWeight: '700' }}>
                            Where in the world?
                        </Typography>
                        <Button color='inherit' sx={{ textTransform: 'none', gap: '5px' }} onClick={toggleMode}  >
                            {darkMode === 'true' ? <LightModeIcon /> : <DarkModeIcon />}
                            {darkMode === 'true' ? <>Light Mode</> : <>Dark Mode</>}
                        </Button>
                    </Toolbar>
                </AppBar>
            </Box>
        </>
    )
}

export default Navbar