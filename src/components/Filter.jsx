import React, { useState, useContext } from 'react';
import { Box, TextField, Stack, Select, MenuItem, InputLabel, FormControl } from '@mui/material';
import ThemeContext from '../context/Theme';
import { createTheme, ThemeProvider } from '@mui/material';

const Filter = ({ onSearch, setSubRegion, subRegion, subRegionsAvailable, sortBy, setSortBy }) => {

    const [searchQuery, setSearchQuery] = useState('');
    const [selectedRegion, setSelectedRegion] = useState('');
    const { darkMode } = useContext(ThemeContext);

    const darkTheme = createTheme({
        palette: {
            mode: darkMode == 'true' ? 'dark' : 'light',
            background: {
                paper: darkMode === 'true' ? 'var(--dark-secondary)' : 'var(--light-secondary)',
                default: darkMode === 'true' ? 'var(--dark-secondary)' : 'var(--light-secondary)'
            }
        }
    });

    const handleSearchChange = (event) => {
        const query = event.target.value;
        setSearchQuery(query);
        onSearch(query, selectedRegion, subRegion, sortBy);
    }

    const handleRegionChange = (event) => {
        const region = event.target.value;
        setSelectedRegion(region);
        onSearch(searchQuery, region, subRegion, sortBy);
    }


    return (
        <>
            <ThemeProvider theme={darkTheme}>
                <Box style={{ maxWidth: '100vw', background: darkMode === 'true' ? 'var(--dark-primary)' : 'var(--light-primary)' }}>
                    <Stack width='90%' margin="auto" padding='4rem 0' direction="row" justifyContent="space-between">

                        <TextField id="outlined-basic" value={searchQuery} onChange={handleSearchChange} label="Searchfor a country ... " variant="outlined"
                            sx={{ width: '30%', borderRadius: '2px' }}
                        />

                        <FormControl sx={{ width: "20%" }}>
                            <InputLabel id="demo-simple-select-label">Region</InputLabel>
                            <Select
                                label="Region"
                                onChange={handleRegionChange}
                            >
                                <MenuItem value={""} > <em>None</em> </MenuItem>
                                <MenuItem value={"Africa"}>Africa</MenuItem>
                                <MenuItem value={"Americas"}>Americas</MenuItem>
                                <MenuItem value={"Asia"}>Asia</MenuItem>
                                <MenuItem value={"Europe"}>Europe</MenuItem>
                                <MenuItem value={"Oceania"}>Oceania</MenuItem>
                            </Select>
                        </FormControl>

                        <FormControl sx={{ width: "20%" }}>
                            <InputLabel>Subregion</InputLabel>
                            <Select
                                value={subRegion}
                                label="Subregion"
                                onChange={(e) => {
                                    const sregion = e.target.value.trim()
                                    setSubRegion(sregion)
                                    onSearch(searchQuery, selectedRegion, sregion, sortBy)
                                }}
                            >
                                <MenuItem value={""}>
                                    <em>None</em>
                                </MenuItem>
                                {subRegionsAvailable[selectedRegion] && subRegionsAvailable[selectedRegion].map((subregionCol, index) => {
                                    return (
                                        <MenuItem key={index} value={`${subregionCol}`}>
                                            {subregionCol}
                                        </MenuItem>
                                    )
                                })}
                            </Select>
                        </FormControl>

                        <FormControl sx={{ width: "20%" }}>
                            <InputLabel id="demo-simple-select-label">Population</InputLabel>
                            <Select
                                label="Population"
                                onChange={(e) => {
                                    setSortBy(e.target.value.trim())
                                    onSearch(searchQuery, selectedRegion, subRegion, e.target.value.trim());
                                }}
                            >
                                <MenuItem value=""><em>None</em></MenuItem>
                                <MenuItem value={"population-desc"}>population (high to low)</MenuItem>
                                <MenuItem value={"population-asce"}>population (low to high)</MenuItem>
                                <MenuItem value={"area-desc"}>Area (high to low)</MenuItem>
                                <MenuItem value={"area-asce"}>Area (low to high)</MenuItem>
                            </Select>
                        </FormControl>
                    </Stack>

                </Box>
            </ThemeProvider>
        </>
    )
}

export default Filter