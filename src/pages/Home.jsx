import { Box,} from '@mui/material'
import React, { useState, useEffect, useContext } from 'react'
import FlagCard from '../components/FlagCard'
import Loader from '../components/Loader'
import Filter from '../components/Filter';
import ThemeContext from '../context/Theme';


const Home = () => {

    const { darkMode } = useContext(ThemeContext);

    const [countryData, setCountryData] = useState([]);
    const [filteredData, setFilteredData] = useState([]);
    const [subRegion, setSubRegion] = useState("");
    const [subRegionsAvailable, setSubRegionsAvailable] = useState({});
    const [sortBy, setSortBy] = useState("");
    const [loadingState, setLoadingState] = useState('true');

    useEffect(() => {
        fetchData();
    }, [])

    async function fetchData() {
        try {
            let res = await fetch("https://restcountries.com/v3.1/all");
            let data = await res.json();
            setCountryData(data);
            setFilteredData(data);
            setLoadingState('false');

            const subRegion = {};
            data.forEach((country) => {
                if (subRegion[country?.region]) {
                    if (!subRegion?.[country.region].includes(country?.subregion))
                        subRegion?.[country.region].push(country?.subregion);
                } else {
                    subRegion[country.region] = [country.subregion];
                }
            });
            setSubRegionsAvailable(subRegion);
        } catch (error) {
            console.log("Error fetching data", error);
        }
    }

    const handleSearch = (query, region, sregion, sortingBy) => {
        console.log(sregion);
        const filtered = countryData.filter((country) => {

            return country.name.official.toLowerCase().includes(query.toLowerCase()) &&
                country.region.toLowerCase().includes(region.toLowerCase()) &&
                (sregion === "" || country.subregion == sregion)
        })
            .sort((a, b) => {
                if (sortingBy === "population-desc") {
                    return b?.population - a?.population;
                } else if (sortingBy === "population-asce") {
                    return a?.population - b?.population;
                } else if (sortingBy === "area-desc") {
                    return b?.area - a?.area;
                } else if (sortingBy === "area-asce") {
                    return a?.area - b?.area;
                }
            })
        setFilteredData(filtered);
    }

    return (
        <>
            <Filter onSearch={handleSearch}
                subRegion={subRegion}
                setSubRegion={setSubRegion}
                subRegionsAvailable={subRegionsAvailable}
                sortBy={sortBy}
                setSortBy={setSortBy}
            />

            <Box
                sx={{
                    background: darkMode === 'true' ? 'var(--dark-primary)' : 'var(--light-primary)',
                    display: 'flex',
                    flexWrap: 'wrap',
                    justifyContent: 'center',
                    gap: '5rem',
                    maxWidth: '100vw',
                    minHeight: 'calc(100vh - 80px)',
                    paddingBottom: "5rem"
                }}
            >
                {
                    loadingState == 'true' ? (
                        <Loader />
                    ) : (
                        filteredData.length !== 0 ? (filteredData.map((country, index) => (
                            <FlagCard
                                key={index}
                                imgSrc={country.flags.svg}
                                name={country.name.common}
                                population={country.population}
                                region={country.region}
                                capital={country.capital}
                                id={country.ccn3}
                                style={{ margin: '10px'}}
                            />
                        ))) : (<p className={darkMode === 'true' ? 'dark notFound' : 'notFound'}> No Data Matched</p>)
                    )
                }

            </Box>
        </>
    )
}

export default Home