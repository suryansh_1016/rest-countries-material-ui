import './App.css'
import Navbar from './components/Navbar'
import ThemeContextProvider from './context/ThemeContextProvider'
import Home from './pages/Home'

function App() {

  return (
    <>
      <ThemeContextProvider>
        <Navbar />
        <Home />
      </ThemeContextProvider>
    </>
  )
}

export default App
